# La documentation sous GNU/Linux.



-----

Les auteurs de logiciels et d'outils internes aux systèmes GNU/Linux documentent l'utilisation et les aspects de ces derniers . Afin de disposer de toutes les informations concernant un outil ou programme certaines commandes vont se montrer primordiales. 

En effet il est très difficile voire impossible pour un être humain de se remémorer un très grand nombres de commandes et d'arguments c'est pourquoi la documentation et l'aide sont nécessaires.

Afin d’évoluer convenablement au sein des écosystèmes GNU/Linux il est strictement nécessaire, avant tout, de savoir effectuer des recherches.


Sommaire :

### 1 - Le manuel (RTFM).

### 2 - La documentation et les wikis.

### 3 - La recherche ciblée.

### 4 - Les Aide-memoires.

**              **


## 1 - Le manuel (RTFM).

RTFM (Read the Fucking Manual) est un acronyme souvent utilisés entre sysadmin pour illustrer un principe assez simple. En effectuant des recherches par nos propres
moyens et avec patience à la source du programme on a de grandes chances de trouver les infos nécessaires.


Lorsqu'on débute il n'est pas aisé de trouver les documentations adéquates car nous ne connaissons tout simplement pas les commandes à utiliser. 

C'est ici qu'une commande très puissante va être de rigueur il s'agit de la commande:  `apropos` ou `man -k` . 

Si par exemple nous souhaitons savoir quels sont les programmes nécessaires à la gestion des volumes logiques (lvm) mais ne connaissons pas la syntaxe exacte des commandes. 

La commande `apropos` ou `man -k` sera très précieuse.

```shell
$ apropos lvm
```
```shell
lvm (8)              - LVM2 tools
lvm-config (8)       - Display and manipulate configuration information
lvm-dumpconfig (8)   - Display and manipulate configuration information
lvm-fullreport (8)   - (sujet inconnu)
lvm-lvpoll (8)       - (sujet inconnu)
lvm.conf (5)         - Configuration file for LVM2
lvm2-activation-generator (8) - generator for systemd units to activate LVM2 volumes on boot
lvmcache (7)         - LVM caching
lvmconf (8)          - LVM configuration modifier
lvmconfig (8)        - Display and manipulate configuration information
lvmdiskscan (8)      - List devices that may be used as physical volumes
lvmdump (8)          - create lvm2 information dumps for diagnostic purposes
lvmetad (8)          - LVM metadata cache daemon
lvmpolld (8)         - LVM poll daemon
lvmraid (7)          - LVM RAID
lvmreport (7)        - LVM reporting and related features
lvmsadc (8)          - LVM system activity data collector
lvmsar (8)           - LVM system activity reporter
lvmsystemid (7)      - LVM system ID
lvmthin (7)          - LVM thin provisioning
perlvms (1)          - VMS-specific documentation for Perl
pvcreate (8)         - Initialize physical volume(s) for use by LVM
pvremove (8)         - Remove LVM label(s) from physical volume(s)
```

Ainsi tous les outils nécessaires  et les entrées de manuels sont listés et nous pouvons alors approfondir nos recherches :

_example:_

```shell
$ man lvm.conf
```


-----


Le puisant outil _man_ utilise en fait un _pager_ (less) afin de nous faire naviguer dans ses pages.
Ces pages se trouvent dans `/usr/share/doc`

Voici quelques touches importantes lorsqu'on lit un manuel (avec less) .


- `/` Permet d'effectuer une recherche.


- `n` Permet de sélectionner le texte de la recherche.


- `q` Permet de quitter man.


- `h` Permet de connaitre les options du pager (less) afin de naviguer plus précisément.


Notons également la présence de chiffres après le nom des entrées de manuels. Ils correspondent aux sections et aux types de pages de documentation. 

La commande `man man` nous donnera les détails.


-----


Lorsque notre connaissance des commandes sera plus étendue nous pourrons visualiser de façon plus succincte, sans aller dans le manuel, la syntaxe et les arguments de commande avec `-h` ou ` --help ` .

Cet argument permet d'avoir une vue d'ensemble de l'utilisation de la commande ou de l'outil. 

Prenons un exemple avec `w` l'outil pour lister les utilisateurs du système.

```shell
$ w --help
```

```shell
Usage:
 w [options]
Options:
 -h, --no-header     do not print header
 -u, --no-current    ignore current process username
 -s, --short         short format
 -f, --from          show remote hostname field
 -o, --old-style     old style output
 -i, --ip-addr       display IP address instead of hostname (if possible)

     --help     display this help and exit
 -V, --version  output version information and exit
```

Ainsi inutile d'aller dans le manuel systématiquement afin de repérer un argument de commande. 

Notons toutefois que le manuel est complet tandis que `-h` ou `--help` ne donnent parfois qu'un résumé des arguments les plus utilisés. 


TIPS : 

Afin de retenir certaines commandes il est conseillé d'utiliser des moyens mnémotechniques dans l'ordre des arguments. 

Exemple : `netstat -tulpn` n'est pas très facile à retenir selon moi je lui préfère `ss -lapute` qui fournit approximativement le même résultat. (Sans misogynie aucune). 

Aussi privilégiez les commandes simples et claires. Vous rencontrerez sur les plateformes beaucoup qui bourrent leurs commandes de regexp alors qu'un argument peut suffire.

Cependant il est absolument primordial de connaitre et différencier le globbing, et les regular expressions (regexp).

Ressources :

https://regexcrossword.com/

https://regex101.com/

https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Introduction




### 2 - La documentation et les wikis

La plupart du temps, la plupart des individus consultent des howtos, des tutos, parfois même des vidéos tuto afin de réaliser des installations et mises en place de systèmes et services.

Les inconvénients aux howtos c'est qu'il ne s'agit pas de documentations explicatives mais d'une suite de procédures, parfois une simple suite de commandes.

Le lecteur ne comprend pas ce qu'il fait et une pléthore de ces lecteurs inondent les forums et les irc avec du : 

>"J'ai tout fait qu'est ce que c’était écrit ça marche pas."

Ces lecteurs parfois oublient de vérifier plusieurs choses :

* La date de publication : En effet les outils, les updates font évoluer les systèmes rapidement et ce qui fonctionnait hier a de grandes chances de fonctionner différemment aujourd'hui. ( ex : cgroups v2 fedora 31)

* La distribution concernée : Il existe de nombreuses distros et parfois même des variantes au sein d'une même famille. Le fonctionnement du système (*systemd*, *openrc*) , les politiques de sécurités (*apparmor*, *selinux*) , les fichiers de confs, etc ...

* Les alternatives : En effet certains sysadmins sont des dinosaures qui ne jurent que par des artefacts. Il peut exister de nouveaux outils plus simples, performants, sécurisés, etc ... ex :  :( *ip* remplace *ifconfig* ) ( *nmtui* est plus pratique que *'/etc/sysconfig/network-scripts/ifcfg-eno1'* )

* La sécurité : Exécuter un script trouvé au hasard sur le Web, arrêter le pare-feu, ou ajouter des dépôts à un système en prod peuvent être ravageurs. Il faut systématiquement appliquer une attitude de prudence et de réflexion.

* L'anglais: Les français ne sont pas nécessairement bien fournis en documentations. Si vous répugnez à lire des docs en anglais envisagez de changer de métier. Sérieusement.

* Le manuel est ton meilleur ami :  À nouveau le manuel regorge de toutes les informations nécessaires. Apprendre à utiliser `less` et à lire correctement sont des elements essentiels.



### 3 - La recherche ciblée.


Il existe de nombreux sites afin d'effectuer des recherches encore une fois l’anglophonie est la clé. Attendre qu'un francophone ait une réponse spécifique sur un forum pour vous peut durer longtemps.

Si vous ne pouvez effecteur des recherches en anglais il va être très difficile pour vous d’évoluer convenablement dans cette branche de métier. 



* Le log c'est la vie: Se pointer sur un forum ou un IRC avec du :

> ça marche pas chez moi comment je fais pour que ça marche ?


C'est excessivement énervant.

Il faudra toujours préciser la distribution , la variante , le but de la manœuvre , les modifications précédentes apportées au système, etc... 

Il faut surtout savoir lire des logs. Un autre chapitre décrit brièvement comment utiliser les outils de logs

**TODO**

Sans logs et descriptions précises il est difficile de se faire aider par un(e) inconnu(e) sur le Web.

Le répertoire `/var/log` est juste la base des bases de tout sysadmin qui se respecte. 

Avec l'apparition de `journalctl` ce répertoire est plus difficile à exploiter mais regorge toujours d'informations vitales.



* Les scripts: Le scripting c'est la vie c'est puissant et bien maitriser ça fait de toi un mage. En revanche inutile de tout scripter.

En effet certains sysadmins et bon scripteurs scriptent tout. Cela posent des problèmes pour ceux qui passent derrière. Revoir des scripts (surtout s'ils sont mauvais) c'est douloureux et ça prend du temps.

Souvent les individus scriptent car il ne connaissent pas bien les outils et les systèmes. ( exemple de scripts `rsync` de 50 lignes alors qu'une commande bien argumentée peut suffire en une ligne. )

Tomber dans la "scriptomanie" peut vous empêcher, à terme d'en apprendre plus sur les outils, les systèmes et vous empêcher des bien les manipuler mais par ailleurs vous faire gagner/perdre un temps fou.

-----


## 4 - Les Aide-mémoires.


Afin de se remémorer des commandes primordiales il est très intéressant de se référer à des aides mémoires. Aussi par exemple lorsqu'on se permet des pauses d’écrans l'on peut consulter ces aide-mémoires (temporairement collées sur le capot de votre laptop car vous êtes acharné par exemple).

Ressources:

https://developers.redhat.com/cheatsheets/

https://www.cheatography.com/

https://linoxide.com/guide/linux-cheat-sheet.png

---

---



- Contact: studentrhcsa@pm.me

- Site: Gitlab

- Date: 08/09/2019





