---     ---
__*Statut : En construction.*__
***     ***

# Initiation aux systèmes GNU_Linux (type CentOS/RHEL/Fedora)  :penguin:

___

Nous proposons quelques notes sur les bases du système GNU/Linux basé CentOS/RHEL/Fedora.
Il s'agit de notes rédigées pendant la préparation d'une certification Red Hat (RHCSA).

Ces chapitres sont en construction.

Enjoy.


## SOMMAIRE


### I - [Rechercher la documentation adaptée.](https://gitlab.com/valentingrebert/initiation_gnu_linux/-/blob/master/Documentation_Aide.md)

### II - Les bonnes pratiques d'installation.

### III - Les bonnes pratiques de maintenance.

### IV - Pratiquer une veille sans relâche.

### V - Documenter son travail.

___
